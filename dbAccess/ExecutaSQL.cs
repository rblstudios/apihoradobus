﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;

namespace dbAccess
{
    public class ExecutaSQL
    {
        public static DataTable ExecutaSqlMysql(string strSQL, Dictionary<string, dynamic> parametros)
        {
            string strServidor = "opmy0005.servidorwebfacil.com";
            string strBanco = "renatoxcar_horadobus";
            string strUsuario = "renat_renan";
            string strSenha = "weAi74*2";

            MySqlConnection oMysqlConnection = default(MySqlConnection);
            string sConnString = "Server=" + strServidor
                               + ";Database=" + strBanco
                               + ";Uid=" + strUsuario
                               + ";Pwd=" + strSenha
                               + ";Port=3306"
                               + ";Default Command Timeout=900;";
            oMysqlConnection = new MySqlConnection(sConnString);
            oMysqlConnection.Open();
            
            MySqlCommand objcmdselect = new MySqlCommand(strSQL, oMysqlConnection);
            foreach (var parametro in parametros)
            {
                objcmdselect.Parameters.AddWithValue(parametro.Key, parametro.Value);
            }
            MySqlDataAdapter objAdapter1 = new MySqlDataAdapter();
            objAdapter1.SelectCommand = objcmdselect;
            DataSet objDataset1 = new DataSet();
            objAdapter1.Fill(objDataset1);

            DataTable dt = objDataset1.Tables[0];

            oMysqlConnection.Close();
            return dt;
        }

        public static int ExecutaSqlMysqlScript(string strSQL, Dictionary<string, dynamic> parametros, Boolean booIDENTITY)
        {
            string strServidor = "opmy0005.servidorwebfacil.com";
            string strBanco = "renatoxcar_horadobus";
            string strUsuario = "renat_renan";
            string strSenha = "weAi74*2";

            MySqlConnection oMysqlConnection = default(MySqlConnection);
            string sConnString = "Server=" + strServidor
                               + ";Database=" + strBanco
                               + ";Uid=" + strUsuario
                               + ";Pwd=" + strSenha
                               + ";Port=3306"
                               + ";Default Command Timeout=900;";
            oMysqlConnection = new MySqlConnection(sConnString);
            oMysqlConnection.Open();

            MySqlCommand objcmdselect = new MySqlCommand(strSQL, oMysqlConnection);
            foreach (var parametro in parametros)
            {
                objcmdselect.Parameters.AddWithValue(parametro.Key, parametro.Value);
            }

            int i = 0;
            if (booIDENTITY)
            {
                objcmdselect.ExecuteScalar();
                i = (int) objcmdselect.LastInsertedId;
            }
            else
            {
                objcmdselect.ExecuteScalar();
            }
            oMysqlConnection.Close();
            return i;

        }
    }
}

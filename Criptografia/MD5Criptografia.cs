﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Criptografia
{
    public class MD5Criptografia
    {
        private static string _chave;

        public string Chave
        {
            set
            {
                _chave = value;
            }
        }

        public string criptografaTexto(string texto)
        {
            try
            {
                return criptografaTexto(texto, _chave);
            }
            catch (Exception ex)
            {
                return "String errada. " + ex.Message;
            }
        }

        public string descriptografaTexto(string texto)
        {
            try
            {
                return descriptografaTexto(texto, _chave);
            }
            catch (Exception ex)
            {
                return "Entrada Errada. " + ex.Message;
            }
        }

        private string criptografaTexto(string texto, string chave)
        {
            try
            {
                chave = "!a@b#c$d5e¨f&g*h(i)j_k+l`m{n}o^p?q:r>s<t,u.v]x[w´y~z";
                TripleDESCryptoServiceProvider objcriptografaSenha = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objcriptoMd5 = new MD5CryptoServiceProvider();

                byte[] byteHash, byteBuff;
                string strTempKey = chave;

                byteHash = objcriptoMd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objcriptoMd5 = null;
                objcriptografaSenha.Key = byteHash;
                objcriptografaSenha.Mode = CipherMode.ECB;

                byteBuff = ASCIIEncoding.ASCII.GetBytes(texto);
                return Convert.ToBase64String(objcriptografaSenha.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
            }
            catch (Exception ex)
            {
                return "Digite os valores Corretamente." + ex.Message;
            }
        }

        private string descriptografaTexto(string strCriptografada, string chave)
        {
            try
            {
                chave = "!a@b#c$d5e¨f&g*h(i)j_k+l`m{n}o^p?q:r>s<t,u.v]x[w´y~z";
                TripleDESCryptoServiceProvider objdescriptografaSenha = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objcriptoMd5 = new MD5CryptoServiceProvider();

                byte[] byteHash, byteBuff;
                string strTempKey = chave;

                byteHash = objcriptoMd5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objcriptoMd5 = null;
                objdescriptografaSenha.Key = byteHash;
                objdescriptografaSenha.Mode = CipherMode.ECB;

                byteBuff = Convert.FromBase64String(strCriptografada);
                string strDecrypted = ASCIIEncoding.ASCII.GetString(objdescriptografaSenha.CreateDecryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
                objdescriptografaSenha = null;

                return strDecrypted;
            }
            catch (Exception ex)
            {
                return "Digite os valores Corretamente." + ex.Message;
            }
        }

        public bool compararStrings(string num01, string num02)
        {
            bool stringValor;
            if (num01.Equals(num02))
            {
                stringValor = true;
            }
            else
            {
                stringValor = false;
            }
            return stringValor;
        }
    }
}

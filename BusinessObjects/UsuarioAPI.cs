﻿namespace BusinessObjects
{
    public class UsuarioAPI
    {
        public int CadastraUsuario(Models.UsuarioAPI usuario)
        {
            dbClass.UsuarioAPI dbUsuarioAPI = new dbClass.UsuarioAPI();
            return dbUsuarioAPI.CadastraUsuario(usuario);
        }

        public Models.UsuarioAPI ValidaUsuario(string usuario, string usuarioSenha)
        {
            dbClass.UsuarioAPI dbUsuarioAPI = new dbClass.UsuarioAPI();
            return dbUsuarioAPI.ValidaUsuario(usuario, usuarioSenha);
        }

        public int GravaToken(int idUsuario, string token, string dataCriacao, string dataExpiracao, string permissoesToken)
        {
            dbClass.UsuarioAPI dbUsuarioAPI = new dbClass.UsuarioAPI();
            return dbUsuarioAPI.GravaToken(idUsuario, token, dataCriacao, dataExpiracao, permissoesToken);
        }
    }
}

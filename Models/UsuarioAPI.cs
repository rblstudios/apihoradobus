﻿using System;
using System.Collections.Generic;

namespace Models
{
    public class UsuarioAPI
    {
        public int id { get; set; }
        public string usuario { get; set; }
        public string senha { get; set; }
        public string salt { get; set; }
        public string ultima_token { get; set; }
        public List<Permissao> permissoes { get; set; }
        public bool podeUtilizarAPI { get; set; }

        public byte[] retornaBytesSalt()
        {
            return Convert.FromBase64String(salt);
        }
    }
}
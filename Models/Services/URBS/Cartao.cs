﻿using System.Collections.Generic;

namespace Models.Services.URBS
{
    public class Cartao
    {
        public string TIPO { get; set; }
        public string SALDO { get; set; }
        public List<LancamentosCartao> LCTOS { get; set; }
    }

    public class LancamentosCartao
    {
        public string DATA { get; set; }
        public string LOCAL { get; set; }
        public string VALOR { get; set; }
    }
}

﻿namespace Models.Services.URBS
{
    public class Veiculos
    {
        public string COD { get; set; }
        public string REFRESH { get; set; }
        public string LAT { get; set; }
        public string LON { get; set; }
        public string CODIGOLINHA { get; set; }
        public string ADAPT { get; set; }
        public string TIPO_VEIC { get; set; }
        public string TABELA { get; set; }
        public string SITUACAO { get; set; }
        public string TCOUNT { get; set; }
    }
}

﻿namespace Models.Services.URBS
{
    public class Linhas
    {
        public string COD { get; set; }
        public string NOME { get; set; }
        public string SOMENTE_CARTAO { get; set; }
        public string CATEGORIA_SERVICO { get; set; }
        public string NOME_COR { get; set; }
    }
}

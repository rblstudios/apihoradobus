﻿namespace Models.Services.URBS
{
    public class ShapeLinha
    {
        public string SHP { get; set; }
        public string LAT { get; set; }
        public string LON { get; set; }
        public string COD { get; set; }
    }
}

﻿using System.Web.Http;

namespace APIHoradoBus.Controllers
{
    public class TokenController : ApiController
    {
        [Authorize]
        [HttpGet]
        public IHttpActionResult ValidaToken()
        {
            return Ok("Token autorizada");
        }
    }
}

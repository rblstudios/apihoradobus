﻿using System;
using System.Web.Http;
using Models;
using Criptografia;

namespace APIHoradoBus.Controllers
{
    public class UsuarioAPIController : ApiController
    {
        private BusinessObjects.UsuarioAPI boUsuarioAPI = new BusinessObjects.UsuarioAPI();
        private BusinessObjects.Chaves boChaves = new BusinessObjects.Chaves();

        [Authorize(Roles = "admin")]
        public IHttpActionResult CadastraUsuario(UsuarioAPI usuario)
        {
            try
            {
                AESCriptografia aesCriptografia = new AESCriptografia();
                byte[] salt = CryptoHelper.GetSalt();
                usuario.salt = Convert.ToBase64String(salt);
                usuario.senha = aesCriptografia.criptografarTexto(usuario.senha, boChaves.BuscaValorChave("aes_criptografia"), salt);

                int idUsuario = boUsuarioAPI.CadastraUsuario(usuario);
                return Ok(new { usuario = idUsuario });
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}

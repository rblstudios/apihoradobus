﻿using System;
using System.Web.Http;
using Models;
using Services.URBS;

namespace APIHoradoBus.Controllers.URBS
{
    public class LinhasController : ApiController
    {
        private BusinessObjects.UsuarioAPI boUsuarioAPI = new BusinessObjects.UsuarioAPI();
        private BusinessObjects.Chaves boChaves = new BusinessObjects.Chaves();

        [Authorize(Roles = "admin")]
        public IHttpActionResult GetLinhas()
        {
            try
            {
                return Ok(URBSService.GetLinhas(boChaves.BuscaValorChave("api_urbs")));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}

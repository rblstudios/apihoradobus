﻿using System;
using System.Web.Http;
using Models;
using Services.URBS;

namespace APIHoradoBus.Controllers.URBS
{
    public class CartaoController : ApiController
    {
        private BusinessObjects.UsuarioAPI boUsuarioAPI = new BusinessObjects.UsuarioAPI();
        private BusinessObjects.Chaves boChaves = new BusinessObjects.Chaves();

        [Authorize(Roles = "admin")]
        [HttpPost]
        public IHttpActionResult GetInfoCartao([FromUri] string codCartao, [FromUri] string cpf)
        {
            try
            {
                return Ok( new { cartao = URBSService.GetInfoCartao(boChaves.BuscaValorChave("api_urbs"), codCartao, cpf)});
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}

﻿using System;
using System.Web.Http;
using Models;
using Services.URBS;

namespace APIHoradoBus.Controllers.URBS
{
    public class VeiculosController : ApiController
    {
        private BusinessObjects.UsuarioAPI boUsuarioAPI = new BusinessObjects.UsuarioAPI();
        private BusinessObjects.Chaves boChaves = new BusinessObjects.Chaves();

        [Authorize(Roles = "admin")]
        public IHttpActionResult GetVeiculos(string linha = "")
        {
            try
            {
                return Ok(new { veiculos = URBSService.GetVeiculos(boChaves.BuscaValorChave("api_urbs"), linha) });
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}

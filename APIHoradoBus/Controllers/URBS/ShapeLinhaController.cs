﻿using System;
using System.Web.Http;
using Models;
using Services.URBS;

namespace APIHoradoBus.Controllers.URBS
{
    public class ShapeLinhaController : ApiController
    {
        private BusinessObjects.Chaves boChaves = new BusinessObjects.Chaves();

        [Authorize(Roles = "admin")]
        public IHttpActionResult GetShapeLinha(string linha)
        {
            try
            {
                return Ok(URBSService.GetShapeLinha(boChaves.BuscaValorChave("api_urbs"), linha));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}

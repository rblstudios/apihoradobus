﻿using System;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Owin.Security.OAuth;
using BusinessObjects;
using System.Security;

namespace APIHoradoBus.Provider
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (context.ClientId == null)
                context.Validated();

            return Task.FromResult<object>(null);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            return Task.Factory.StartNew(() =>
            {
                var usuarioNome = context.UserName;
                var usuarioSenha = context.Password;
                var autenticacao = new UsuarioAPI();
                // Validamos o usuário
                var usuarioAutenticado = autenticacao.ValidaUsuario(usuarioNome, usuarioSenha);

                // Se o usuário retornou do banco, continuamos o processo de validação
                // Se não, retornamos mensagem dizendo que o usuário é inválido
                if (usuarioAutenticado != null)
                {
                    // Se o usuário tem permissões, geramos a token
                    // Se não, retornamos mensagem dizendo que o usuário não tem permissões
                    if (usuarioAutenticado.permissoes != null && usuarioAutenticado.permissoes.Count > 0)
                    {
                        // Salvamos o nome e id do usuário para montarmos a token
                        var claims = new List<Claim>()
                        {
                            new Claim(ClaimTypes.Sid, Convert.ToString(usuarioAutenticado.id)),
                            new Claim(ClaimTypes.Name, usuarioAutenticado.usuario),
                        };
                        
                        // E adicionamos que roles a token vai permitir acessar
                        // EX: a role "admin" permite acessar vários métodos da API
                        foreach (Models.Permissao permissao in usuarioAutenticado.permissoes)
                        {
                            claims.Add(new Claim(ClaimTypes.Role, permissao.descricao));
                        }

                        ClaimsIdentity oAuthIdentity = new ClaimsIdentity(claims,
                                    context.Options.AuthenticationType);

                        var properties = CriaPropriedades(usuarioAutenticado.id, usuarioAutenticado.usuario);
                        var ticket = new AuthenticationTicket(oAuthIdentity, properties);
                        context.Validated(ticket);
                    }
                    else
                    {
                        context.SetError("permissao_invalida", "Usuário não tem permissão para acessar este método");
                    }
                }
                else
                {
                    context.SetError("usuario_invalido", "Usuário ou senha incorretos");
                }
            });
        }

        public static AuthenticationProperties CriaPropriedades(int idUsuario, string usuario)
        {
            IDictionary<string, string> dados = new Dictionary<string, string>
            {
                {"id_usuario", idUsuario.ToString()},
                {"usuario", usuario}
            };

            return new AuthenticationProperties(dados);
        }

        /**
         * Método que retorna a token para o client 
         * Este método foi reescrito para salvarmos a token no banco
         **/
        public override Task TokenEndpointResponse(OAuthTokenEndpointResponseContext context)
        {
            // Buscamos o id do usuário que foi salvo nas propriedades acima
            int idUsuario = int.Parse(context.Properties.Dictionary["id_usuario"]);
            var servicoUsuario = new UsuarioAPI();

            // Pegamos os claims para então pegarmos as roles que esta token dá acesso
            string permissoesToken = "";

            IEnumerable<Claim> claims = context.Identity.Claims;
            int iRegistro = 0;
            foreach (Claim claim in claims)
            {
                // Buscamos somente as roles e concatenamos os valores em uma string
                if (claim.Type == ClaimTypes.Role)
                {
                    if (iRegistro > 0)
                    {
                        permissoesToken += ",";
                    }

                    permissoesToken += claim.Value;
                    iRegistro++;
                }
            }

            // Gravamos a token no banco de dados
            servicoUsuario.GravaToken(idUsuario, context.AccessToken, 
                context.Properties.IssuedUtc.Value.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss"), 
                context.Properties.ExpiresUtc.Value.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss"), permissoesToken);

            return base.TokenEndpointResponse(context);
        }
    }
}
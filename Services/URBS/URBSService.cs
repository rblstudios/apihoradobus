﻿using Models.Services.URBS;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;

namespace Services.URBS
{
    public class URBSService
    {
        private const string URL_MAIN = "https://transporteservico.urbs.curitiba.pr.gov.br";
        private const string URL_GETLINHAS = "getLinhas.php";
        private const string URL_GETSHAPELINHA = "getShapeLinha.php";
        private const string URL_GETVEICULOS = "getVeiculos.php";
        private const string URL_GETINFOCARTAO = "getInfoCartao.php";

        public static List<Linhas> GetLinhas(string apiAcessCode)
        {
            var client = new RestClient(URL_MAIN);
            var request = new RestRequest(URL_GETLINHAS + "?c=" + apiAcessCode, Method.GET);

            IRestResponse response = client.Execute(request);
            var content = response.Content; 

            return JsonConvert.DeserializeObject<List<Linhas>>(content);
        }

        public static List<ShapeLinha> GetShapeLinha(string apiAcessCode, string linha)
        {
            var client = new RestClient(URL_MAIN);
            var request = new RestRequest(URL_GETSHAPELINHA + "?c=" + apiAcessCode
                + "&linha=" + WebUtility.UrlEncode(linha), Method.GET);

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            return JsonConvert.DeserializeObject<List<ShapeLinha>>(content);
        }

        public static List<Veiculos> GetVeiculos(string apiAcessCode, string linha)
        {
            var client = new RestClient(URL_MAIN);
            var request = new RestRequest(URL_GETVEICULOS + "?c=" + apiAcessCode 
                + "&linha=" + WebUtility.UrlEncode(linha), Method.GET);

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            return JsonConvert.DeserializeObject<List<Veiculos>>(content);
        }

        public static Cartao GetInfoCartao(string apiAcessCode, string codCartao, string cpf)
        {
            Cartao cartao = new Cartao();

            var client = new RestClient(URL_MAIN);
            var request = new RestRequest(URL_GETINFOCARTAO + "?c=" + apiAcessCode, Method.POST);
            request.AddParameter("ct", codCartao);
            request.AddParameter("cpf", cpf);
            request.AddParameter("json", 1); // 1 para receber dados em JSON, 0 para receber dados formatados

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            try
            {
                cartao = JsonConvert.DeserializeObject<Cartao>(content);
            }
            catch (Exception)
            {
                cartao = null;
            }

            return cartao;
        }
    }
}

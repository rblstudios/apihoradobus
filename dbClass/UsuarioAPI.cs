﻿using dbAccess;
using System.Collections.Generic;
using System.Data;
using Criptografia;

namespace dbClass
{
    public class UsuarioAPI
    {
        public int CadastraUsuario(Models.UsuarioAPI usuario)
        {
            Dictionary<string, dynamic> parametros = new Dictionary<string, dynamic>();
            string strSQL = "INSERT INTO USUARIOS_API (USUARIO, SENHA, SALT, PODE_UTILIZAR) VALUES (@usuario, @senha, @salt, @podeUtilizar)";
            parametros["@usuario"] = usuario.usuario;
            parametros["@senha"] = usuario.senha;
            parametros["@salt"] = usuario.salt;
            parametros["@podeUtilizar"] = usuario.podeUtilizarAPI == true ? "1" : "0";
          
            return ExecutaSQL.ExecutaSqlMysqlScript(strSQL, parametros, true);
        }

        public Models.UsuarioAPI ValidaUsuario(string usuario, string usuarioSenha)
        {
            // Buscamos o usuário da api e verificamos se ele é válido
            Dictionary<string, dynamic> parametros = new Dictionary<string, dynamic>();
            string strSQL = "SELECT ID, USUARIO, ULTIMA_TOKEN, SALT" 
                            + " FROM USUARIOS_API" 
                            + " WHERE USUARIO = @usuario" 
                            + " AND PODE_UTILIZAR = 1";
            parametros["@usuario"] = usuario;

            DataTable dtUsuario = ExecutaSQL.ExecutaSqlMysql(strSQL, parametros);

            if (dtUsuario.Rows.Count > 0)
            {
                Models.UsuarioAPI usuarioAPI = new Models.UsuarioAPI();
                usuarioAPI.id = int.Parse(dtUsuario.Rows[0]["ID"].ToString());
                usuarioAPI.usuario = dtUsuario.Rows[0]["USUARIO"].ToString();
                usuarioAPI.ultima_token = dtUsuario.Rows[0]["ULTIMA_TOKEN"].ToString();
                usuarioAPI.salt = dtUsuario.Rows[0]["SALT"].ToString();

                Chaves dbChaves = new Chaves();
                AESCriptografia aesCriptografia = new AESCriptografia();
                string senhaCriptografada = aesCriptografia.criptografarTexto(usuarioSenha,
                    dbChaves.BuscaValorChave("aes_criptografia"), usuarioAPI.retornaBytesSalt());

                // Buscamos o usuário da api e verificamos se ele é válido
                parametros = new Dictionary<string, dynamic>();
                strSQL = "SELECT ID, USUARIO, ULTIMA_TOKEN, SALT"
                                + " FROM USUARIOS_API"
                                + " WHERE USUARIO = @usuario"
                                + " AND SENHA = @senhaCriptografada"
                                + " AND PODE_UTILIZAR = 1";
                parametros["@usuario"] = usuario;
                parametros["@senhaCriptografada"] = senhaCriptografada;

                DataTable dtUsuarioValidado = ExecutaSQL.ExecutaSqlMysql(strSQL, parametros);
                // Se encontrou o usuário, monta a model com os dados do usuário
                // Se não retorna nulo
                if (dtUsuarioValidado.Rows.Count > 0)
                {
                    Models.UsuarioAPI usuarioAPIValidado = new Models.UsuarioAPI();
                    usuarioAPIValidado.id = int.Parse(dtUsuario.Rows[0]["ID"].ToString());
                    usuarioAPIValidado.usuario = dtUsuario.Rows[0]["USUARIO"].ToString();
                    usuarioAPIValidado.ultima_token = dtUsuario.Rows[0]["ULTIMA_TOKEN"].ToString();

                    // Query de busca de permissões deste usuário
                    strSQL = "SELECT USRPERM.ID_PERMISSAO, PERM.DESCRICAO"
                             + " FROM USUARIOS_API_PERMISSOES USRPERM"
                             + " INNER JOIN PERMISSOES PERM ON(USRPERM.ID_PERMISSAO = PERM.ID)"
                             + " WHERE USRPERM.ID_USUARIO_API = @idUsuarioAPI";

                    parametros = new Dictionary<string, dynamic>();
                    parametros["@idUsuarioAPI"] = usuarioAPIValidado.id;

                    // Realizamos a busca
                    DataTable dtPermissoes = ExecutaSQL.ExecutaSqlMysql(strSQL, parametros);
                    // Se encontramos permissões para este usuário, montamos a lista de permissões e gravamos na model
                    // Se não encontramos, as permissões são retornadas nulo
                    if (dtPermissoes.Rows.Count > 0)
                    {
                        List<Models.Permissao> permissoesList = new List<Models.Permissao>();
                        foreach (DataRow drPermissao in dtPermissoes.Rows)
                        {
                            Models.Permissao permissao = new Models.Permissao();
                            permissao.id = int.Parse(drPermissao["ID_PERMISSAO"].ToString());
                            permissao.descricao = drPermissao["DESCRICAO"].ToString();
                            permissoesList.Add(permissao);
                        }
                        usuarioAPIValidado.permissoes = permissoesList;
                    }

                    return usuarioAPIValidado;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            } 
        }

        public int GravaToken(int idUsuario, string token, string dataCriacao, string dataExpiracao, string permissoesToken)
        {
            bool gravouHistorico = false;
            bool gravouTokenUsuario = false;
            int resultado = 0;

            Dictionary<string, dynamic> parametros = new Dictionary<string, dynamic>();

            string strSQL = "UPDATE USUARIOS_API"
                            + " SET ULTIMA_TOKEN = @token,"
                            + " DATA_GERACAO_TOKEN = @dataCriacao" 
                            + " WHERE ID = @idUsuario";
            parametros["@idUsuario"] = idUsuario;
            parametros["@token"] = token;
            parametros["@dataCriacao"] = dataCriacao;
            parametros["@dataExpiracao"] = dataExpiracao;
            parametros["@permissoesToken"] = permissoesToken;
            gravouTokenUsuario = ExecutaSQL.ExecutaSqlMysqlScript(strSQL, parametros, false) == 1;

            strSQL = "INSERT INTO HISTORICO_TOKENS (ID_USUARIO, TOKEN, PERMISSOES_TOKEN, DATA_CRIACAO, DATA_EXPIRACAO) VALUES (@idUsuario, @token, @permissoesToken, @dataCriacao, @dataExpiracao)";
            gravouHistorico = ExecutaSQL.ExecutaSqlMysqlScript(strSQL, parametros, false) == 1;

            if (gravouTokenUsuario && gravouHistorico)
            {
                resultado = 1;
            }
            
            return resultado;
        }
    }
}

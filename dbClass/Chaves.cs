﻿using dbAccess;
using System;
using System.Collections.Generic;
using System.Data;

namespace dbClass
{
    public class Chaves
    {
        public string BuscaValorChave(string chave)
        {
            string valorChave = null;

            Dictionary<string, dynamic> parametros = new Dictionary<string, dynamic>();
            string strSQL = "SELECT VALOR FROM CHAVES WHERE CHAVE = @chave";

            parametros["@chave"] = chave;

            DataTable dtChave = ExecutaSQL.ExecutaSqlMysql(strSQL, parametros);

            if (dtChave.Rows.Count > 0)
            {
                valorChave = dtChave.Rows[0]["VALOR"].ToString();
            }

            return valorChave;
        }
    }
}
